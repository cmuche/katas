package muche.kata04;

public class App
{

  public static void main(String[] args) throws Exception
  {
    WeatherReader weatherReader = new WeatherReader();
    FootballReader footballReader = new FootballReader();

    Object bestDay = weatherReader.findBestMatch();
    System.out.println("Best day: " + bestDay);

    Object bestTeam = footballReader.findBestMatch();
    System.out.println("Best team: " + bestTeam);
  }
}
