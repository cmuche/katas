package muche.kata04;

import java.util.ArrayList;

public class FootballReader extends ListReader
{

  @Override
  public ArrayList<String> readTextLines() throws Exception
  {
    return readTextLines("football.dat");
  }

  @Override
  public boolean isValidLine(String line)
  {
    String team = getStringValue(line, 1);
    Integer valFor = getIntVaue(line, 6);
    Integer valAgainst = getIntVaue(line, 8);

    return noneOfThemIsNull(team, valFor, valAgainst);
  }

  @Override
  protected ILineRater getLineRater()
  {
    return new ILineRater()
    {

      @Override
      public Object getKey(String line)
      {
        return getStringValue(line, 1);
      }

      @Override
      public float getScore(String line)
      {
        Integer valFor = getIntVaue(line, 6);
        Integer valAgainst = getIntVaue(line, 8);
        int diff = Math.abs(valFor - valAgainst);
        return diff;
      }
    };
  }

}
