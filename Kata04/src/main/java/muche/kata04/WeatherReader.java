package muche.kata04;

import java.util.ArrayList;

public class WeatherReader extends ListReader
{

  @Override
  public ArrayList<String> readTextLines() throws Exception
  {
    return readTextLines("weather.dat");
  }

  @Override
  public boolean isValidLine(String line)
  {
    Float day = getFloatValue(line, 0);
    Float maxTemp = getFloatValue(line, 1);
    Float minTemp = getFloatValue(line, 2);

    return noneOfThemIsNull(day, minTemp, maxTemp);
  }

  @Override
  protected ILineRater getLineRater()
  {
    return new ILineRater()
    {

      @Override
      public Object getKey(String line)
      {
        return getIntVaue(line, 0);
      }

      @Override
      public float getScore(String line)
      {
        float maxTemp = getFloatValue(line, 1);
        float minTemp = getFloatValue(line, 2);
        float spread = maxTemp - minTemp;
        return spread;
      }
    };
  }

}
