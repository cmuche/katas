package muche.kata04;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Predicate;

public abstract class ListReader
{

  public abstract ArrayList<String> readTextLines() throws Exception;

  public abstract boolean isValidLine(String line);

  protected abstract ILineRater getLineRater();

  protected boolean noneOfThemIsNull(Object... objects)
  {
    for (Object o : objects)
      if (o == null)
        return false;

    return true;
  }

  public Object findBestMatch() throws Exception
  {
    Object bestKey = null;
    float bestScore = Float.MAX_VALUE;

    ArrayList<String> lines = readTextLines();
    ILineRater lineRater = getLineRater();

    for (String line : lines)
    {
      if (!isValidLine(line))
        continue;

      Object thisKey = lineRater.getKey(line);
      float thisScore = lineRater.getScore(line);

      if (thisScore < bestScore)
      {
        bestScore = thisScore;
        bestKey = thisKey;
      }
    }

    return bestKey;
  }

  protected ArrayList<String> readTextLines(String filename) throws Exception
  {
    FileReader fileReader = new FileReader(filename);
    BufferedReader bufferedReader = new BufferedReader(fileReader);
    ArrayList<String> lines = new ArrayList<>();
    String line = null;
    while ((line = bufferedReader.readLine()) != null)
      lines.add(line);
    bufferedReader.close();
    return lines;
  }

  public ArrayList<String> splitValues(String line)
  {
    String[] splitted = line.split(" ");
    ArrayList<String> splittedList = new ArrayList<>(Arrays.asList(splitted));

    splittedList.removeIf(new Predicate<String>()
    {
      @Override
      public boolean test(String t)
      {
        return t.isEmpty();
      }
    });

    return splittedList;
  }

  protected Float getFloatValue(String line, int index)
  {
    ArrayList<String> splitValues = splitValues(line);

    if (index >= splitValues.size())
      return null;

    String stringValue = splitValues.get(index).replace("*", "");
    try
    {
      return Float.valueOf(stringValue);
    }
    catch (NumberFormatException ex)
    {
      return null;
    }
  }

  protected Integer getIntVaue(String line, int index)
  {
    ArrayList<String> splitValues = splitValues(line);

    if (index >= splitValues.size())
      return null;

    String stringValue = splitValues.get(index);
    try
    {
      return Integer.valueOf(stringValue);
    }
    catch (NumberFormatException ex)
    {
      return null;
    }
  }

  protected String getStringValue(String line, int index)
  {
    ArrayList<String> splitValues = splitValues(line);

    if (index >= splitValues.size())
      return null;

    return splitValues.get(index);
  }
}
