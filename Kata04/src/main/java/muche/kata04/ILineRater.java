package muche.kata04;

public interface ILineRater
{

  public Object getKey(String line);

  public float getScore(String line);
}
