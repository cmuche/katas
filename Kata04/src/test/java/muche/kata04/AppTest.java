package muche.kata04;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AppTest
{

  private WeatherReader weatherReader;
  private FootballReader footballReader;

  @Before
  public void init()
  {
    weatherReader = new WeatherReader();
    footballReader = new FootballReader();
  }

  @Test
  public void itShouldSplitValues()
  {
    Assert.assertEquals("foo", weatherReader.splitValues("foo bar").get(0));
    Assert.assertEquals("bar", weatherReader.splitValues("foo bar").get(1));
    Assert.assertEquals("foo", weatherReader.splitValues("foo    bar").get(0));
    Assert.assertEquals("bar", weatherReader.splitValues("foo    bar").get(1));
    Assert.assertEquals("bar", weatherReader.splitValues("   foo    bar     ").get(1));
    Assert.assertEquals("bar", weatherReader.splitValues("   foo    bar    baz ").get(1));
    Assert.assertEquals("baz", weatherReader.splitValues("   foo    bar    baz ").get(2));
  }

  @Test
  public void itShouldReadFileIntoList() throws Exception
  {
    Assert.assertEquals("  Dy MxT   MnT   AvT   HDDay  AvDP 1HrP TPcpn WxType PDir AvSp Dir MxS SkyC MxR MnR AvSLP", weatherReader.readTextLines().get(0));
    Assert.assertEquals("   1  88    59    74          53.8       0.00 F       280  9.6 270  17  1.6  93 23 1004.5", weatherReader.readTextLines().get(2));
  }

  @Test
  public void itShouldGetFloatValues()
  {
    Assert.assertEquals(1.5f, weatherReader.getFloatValue(" 1.5 2.5 3.5", 0), 0.01f);
    Assert.assertEquals(2.5f, weatherReader.getFloatValue(" 1.5 2.5 3.5", 1), 0.01f);
    Assert.assertEquals(3.5f, weatherReader.getFloatValue(" 1.5 2.5 3.5", 2), 0.01f);
    Assert.assertEquals(1.5f, weatherReader.getFloatValue(" 1.5*    *2.5    *3.5*", 0), 0.01f);
    Assert.assertEquals(2.5f, weatherReader.getFloatValue(" 1.5*    *2.5    *3.5*", 1), 0.01f);
    Assert.assertEquals(3.5f, weatherReader.getFloatValue(" 1.5*    *2.5    *3.5*", 2), 0.01f);
    Assert.assertNull(weatherReader.getFloatValue(" foo ", 0));
    Assert.assertNull(weatherReader.getFloatValue("1 2 3", 3));
    Assert.assertNull(weatherReader.getFloatValue("1 2 3", 10));
  }

  @Test
  public void itShouldTestWeatherLine()
  {
    Assert.assertTrue(weatherReader.isValidLine("1.1 2.2 3.3"));
    Assert.assertTrue(weatherReader.isValidLine("1.1* 2.2 3.3"));
    Assert.assertTrue(weatherReader.isValidLine("1.1 2.2* 3.3"));
    Assert.assertTrue(weatherReader.isValidLine("1.1 2.2 3*"));
    Assert.assertFalse(weatherReader.isValidLine("foo 2.2 3.3"));
    Assert.assertFalse(weatherReader.isValidLine("1.1 foo 3.3"));
    Assert.assertFalse(weatherReader.isValidLine("1.1 2.2 foo"));
  }

  @Test
  public void itShouldFindBestDay() throws Exception
  {
    Assert.assertEquals(14, (int) weatherReader.findBestMatch());
  }

  @Test
  public void itShouldTestFootballLine()
  {
    Assert.assertTrue(footballReader.isValidLine("    6. Chelsea         38    17  13   8    66  -  38    64"));
    Assert.assertFalse(footballReader.isValidLine("    6. Chelsea         38    17  13   8    foo  -  38    64"));
    Assert.assertFalse(footballReader.isValidLine("Chelsea         38    17  13   8    66  -  38    64"));
  }

  @Test
  public void itShouldFindBestTeam() throws Exception
  {
    Assert.assertEquals("Aston_Villa", footballReader.findBestMatch());
  }
}
