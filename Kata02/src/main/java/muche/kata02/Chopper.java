package muche.kata02;

import java.util.Arrays;

public class Chopper
{
    private static final int ERR_VALUE = -1;

    public static int getMiddleValue(int[] array)
    {
        int middleIdx = getMiddleIndex(array);
        return array[middleIdx];
    }

    private static int getMiddleIndex(int[] array)
    {
        return array.length / 2;
    }

    public static int[] getSubArray(int[] array, boolean high)
    {
        int middleIdx = getMiddleIndex(array);

        if (high)
            return Arrays.copyOfRange(array, middleIdx, array.length);
        else
            return Arrays.copyOfRange(array, 0, middleIdx);
    }

    private static int chop(int startOffset, int value, int[] array)
    {
        int middleValue = getMiddleValue(array);
        int middleIdx = getMiddleIndex(array);

        if (array.length == 1)
            return middleValue == value ? startOffset : ERR_VALUE;

        if (value < middleValue)
            return chop(startOffset, value, getSubArray(array, false));

        return chop(startOffset + middleIdx, value, getSubArray(array, true));
    }

    public static int chop(int value, int[] array)
    {
        if (array.length == 0)
            return ERR_VALUE;

        return chop(0, value, array);
    }
}
