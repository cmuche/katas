package muche.kata02;

import org.junit.Assert;
import org.junit.Test;

public class AppTest
{

  @Test
  public void itShouldChop()
  {
    Assert.assertEquals(-1, Chopper.chop(3, new int[]{}));
    Assert.assertEquals(-1, Chopper.chop(3, new int[]{1}));
    Assert.assertEquals(0, Chopper.chop(1, new int[]{1}));

    Assert.assertEquals(0, Chopper.chop(1, new int[]{1, 3, 5}));
    Assert.assertEquals(1, Chopper.chop(3, new int[]{1, 3, 5}));
    Assert.assertEquals(2, Chopper.chop(5, new int[]{1, 3, 5}));
    Assert.assertEquals(-1, Chopper.chop(0, new int[]{1, 3, 5}));
    Assert.assertEquals(-1, Chopper.chop(2, new int[]{1, 3, 5}));
    Assert.assertEquals(-1, Chopper.chop(4, new int[]{1, 3, 5}));
    Assert.assertEquals(-1, Chopper.chop(6, new int[]{1, 3, 5}));

    Assert.assertEquals(0, Chopper.chop(1, new int[]{1, 3, 5, 7}));
    Assert.assertEquals(1, Chopper.chop(3, new int[]{1, 3, 5, 7}));
    Assert.assertEquals(2, Chopper.chop(5, new int[]{1, 3, 5, 7}));
    Assert.assertEquals(3, Chopper.chop(7, new int[]{1, 3, 5, 7}));
    Assert.assertEquals(-1, Chopper.chop(0, new int[]{1, 3, 5, 7}));
    Assert.assertEquals(-1, Chopper.chop(2, new int[]{1, 3, 5, 7}));
    Assert.assertEquals(-1, Chopper.chop(4, new int[]{1, 3, 5, 7}));
    Assert.assertEquals(-1, Chopper.chop(6, new int[]{1, 3, 5, 7}));
    Assert.assertEquals(-1, Chopper.chop(8, new int[]{1, 3, 5, 7}));
  }

  @Test
  public void itShouldGetSubArray()
  {
    Assert.assertArrayEquals(new int[]{3, 4}, Chopper.getSubArray(new int[]{1, 2, 3, 4}, true));
    Assert.assertArrayEquals(new int[]{1, 2}, Chopper.getSubArray(new int[]{1, 2, 3, 4}, false));
    Assert.assertArrayEquals(new int[]{2, 3}, Chopper.getSubArray(new int[]{1, 2, 3}, true));
    Assert.assertArrayEquals(new int[]{1}, Chopper.getSubArray(new int[]{1, 2, 3}, false));
  }

  @Test
  public void itShouldCalculateMiddleValue()
  {
    Assert.assertEquals(1, Chopper.getMiddleValue(new int[]{1}));
    Assert.assertEquals(2, Chopper.getMiddleValue(new int[]{1, 2, 3}));
    Assert.assertEquals(3, Chopper.getMiddleValue(new int[]{1, 2, 3, 4}));
  }
}
